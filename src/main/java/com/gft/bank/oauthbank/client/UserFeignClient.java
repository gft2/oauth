package com.gft.bank.oauthbank.client;

import com.gft.bank.domain.model.User;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="user-bank")
public interface UserFeignClient {

    @RequestMapping("/users/search/findUserByUserName")
    public User getUserByUserName(@RequestParam String  userName);

}
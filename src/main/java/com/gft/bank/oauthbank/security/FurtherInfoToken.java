package com.gft.bank.oauthbank.security;

import java.util.HashMap;
import java.util.Map;

import com.gft.bank.domain.model.User;
import com.gft.bank.oauthbank.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

@Component
public class FurtherInfoToken implements TokenEnhancer {

    @Autowired
    IUserService UserService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        Map<String,Object> info=new HashMap<String,Object>();
        User user=UserService.findByUserName(authentication.getName());
        info.put("userName", user.getUserName());
        info.put("roles", user.getRoles());
        info.put("id", user.getId());
        ((DefaultOAuth2AccessToken)accessToken).setAdditionalInformation(info);
        return accessToken;
    }

    

}
package com.gft.bank.oauthbank.service;

import java.util.List;
import java.util.stream.Collectors;

import com.gft.bank.domain.model.User;
import com.gft.bank.oauthbank.client.UserFeignClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService, IUserService {

    @Autowired
    UserFeignClient feignService;

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        User userBank = null;
        List<GrantedAuthority> authorities =null;
        try {
            userBank = feignService.getUserByUserName(userName);
            if (userBank == null) {
                log.error("Error login with user'" + userName + "' not found");
                throw new UsernameNotFoundException("Error login with user'" + userName + "' not found");
            }
            authorities = userBank.getRoles().stream()
                    .map(r -> new SimpleGrantedAuthority(r.getName()))
                    .peek(authority -> log.info("Role: " + authority.getAuthority())).collect(Collectors.toList());
            log.info("login for user: '"+userName+"' sucessfully");
            return new org.springframework.security.core.userdetails.User(userBank.getUserName(),
                    userBank.getPassword(), userBank.isIsEnable(), true, true, true, authorities);
        } catch (UsernameNotFoundException e) {
            log.error(e.getMessage());
        }
        return null;

    }

    @Override
    public User findByUserName(String userName) {
        return feignService.getUserByUserName(userName);
    }

}
package com.gft.bank.oauthbank.service;

import com.gft.bank.domain.model.User;

public interface IUserService {

    public User findByUserName(String userName);
}